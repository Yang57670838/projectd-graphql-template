import { UserStore } from "../user";

export interface Store {
    user: UserStore;
}
