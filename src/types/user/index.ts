import { actions as ActionTypes } from "../../constants/action";

export interface FetchTokenSuccessAction {
  type: ActionTypes.FETCH_TOKEN_SUCCESS;
  token: string;
}

export interface RefreshTokenRequestedAction {
  type: ActionTypes.REFRESH_TOKEN_REQUESTED;
  currentActionKey: string;
}

export interface RefreshTokenSuccessAction {
  type: ActionTypes.REFRESH_TOKEN_SUCCESS;
  token: string;
}

export interface RefreshTokenResetAction {
  type: ActionTypes.RESET_REFRESH_TOKEN;
}

export interface UndefinedUserAction {
  type: ActionTypes.UNDEFINED;
}

export type UserActions =
  | FetchTokenSuccessAction
  | RefreshTokenRequestedAction
  | RefreshTokenSuccessAction
  | RefreshTokenResetAction
  | UndefinedUserAction;

export interface UserStore {
  token: string;
  error: boolean;
  tokenRetryAttempt: number;
  tokenExpired: boolean;
  currentActionKey: string;
}
