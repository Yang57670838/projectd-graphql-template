export type RetroItemType =
  | "Needs Improvement"
  | "Worked Well"
  | "New Ideas"
  | "Recognition";

export interface RetroResult {
  id: string;
  company: string;
  squad: string;
  createdAt: number;
  active: boolean;
  workedWell: Array<RetroItem>;
  needsImprovement: Array<RetroItem>;
  newIdeas: Array<RetroItem>;
  recognition: Array<RetroItem>;
}

export interface RetroItem {
  content: string;
  supportNo: number;
}
