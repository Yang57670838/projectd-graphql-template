import { createTheme } from "@mui/material/styles";
import { theme1 } from "./theme1";
import { theme2 } from "./theme2";

export const baseTheme1 = createTheme(theme1);

export const baseTheme2 = createTheme(theme2);
