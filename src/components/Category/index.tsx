import { useMutation } from "@tanstack/react-query";
import { addProducts } from "../../api/product/add";

const Category = () => {
  const mutation = useMutation(addProducts, {
    onSuccess: (data, variables, context) => {
      // add success toast message
      console.log("success POST new projects");
    },
    onError: (error, variables, context) => {
      // add error toast message
      console.log("error POST new projects");
    }
  });

  const add = () => {
    mutation.mutate();
  };

  if (mutation.isLoading) return <>is adding....</>;
  return (
    <>
      <button onClick={add}>Add New Products</button>
      {mutation.isError ? (
        <div>An error occurred: {(mutation.error as any).message}</div>
      ) : null}

      {mutation.isSuccess ? <div>Products added!</div> : null}
    </>
  );
};
export default Category;
