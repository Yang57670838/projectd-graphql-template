import { useQuery } from "@tanstack/react-query";
import { useDispatch } from "react-redux";
import { fetchProducts } from "../../api/product/fetch";
import { userActions } from "../../store/actions";

const ProductsMgt = () => {
  const dispatch = useDispatch();
  const { data, isLoading, error } = useQuery(
    ["products"],
    fetchProducts,
    {
      cacheTime: 360000,
      retry: false,
      onSuccess: (data: Array<any>) => console.log("green toast message for success fetch products"),
      onError: (error: any) => {
        if (error.response.status === 401) {
          dispatch(userActions.refreshTokenRequestedAction("products"));
        }
      }
    }
  );

  if (isLoading) return <>is loading....</>;
  return <div>ProductsMgt</div>;
};
export default ProductsMgt;
