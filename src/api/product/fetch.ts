import { baseRequest } from "../index";

export const fetchProducts = () => {
    return baseRequest.get("/products");
};
