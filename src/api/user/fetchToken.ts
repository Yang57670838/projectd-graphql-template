import { baseRequest } from "../index";

export const fetchToken = () => {
    return baseRequest.get("/oauth/token");
};
